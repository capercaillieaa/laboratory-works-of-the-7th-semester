import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

#beg_params
#------------------
X_0 = 1
Y_0 = 1

A = 1
B = 3

T = 30
#------------------

def brusselator_SDE(beg_conditions, times):
    x, y = beg_conditions
    return [
        A - (B + 1)*x + (x**2)*y,
        B*x - (x**2)*y
    ]

def solve_SDE(times, beg_conditions):
    return odeint(brusselator_SDE, beg_conditions, times)

def show_plot(times, x_concentration, y_concentration):
    #plt.plot(times, x_concentration, color="blue")
    #plt.plot(times, y_concentration, color="red")
    plt.plot(x_concentration, y_concentration, color="red")
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.legend(["X", "Y"])
    plt.grid(True)
    plt.show()

def main():
    times = np.linspace(0, T, T*100)
    beg_conditions = [X_0, Y_0]
    SDE_solution = solve_SDE(times, beg_conditions)
    x_concentration = SDE_solution[:, 0]
    y_concentration = SDE_solution[:, 1]
    show_plot(times, x_concentration, y_concentration)

main()