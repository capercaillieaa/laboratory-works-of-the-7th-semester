import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

#beg_params
#------------------
pop_size    = 5000  # численность популяции
growth      = 0     # коэфф. прироста
reduction   = 0     # коэфф. убыли

beta_SI     = 9     # частота контактов воспиримчивых с больными и инфицированными
beta_AI     = 8     # частота контактов особей с иммунитетом с больными и инфицированными
beta_SA     = 0.8   # частота приобретения иммунитета

sigma_SI    = 0     # частота возврата потери иммунитета особям, имеющих таковой после заражения
sigma_AI    = 6     # частота возврата иммунитета больным
sigma_SA    = 0.9   # частота потери иммунитета
sigma_IR    = 1     # частота выздоровления
sigma_RS    = 1.5   # частота неполучения иммунитета
sigma_RA    = 1.5   # частота получения иммунитета

TIME        = 2     # продолжительность наблюдений

A_0         = 0.69  # начальная доля особей с иммунитетом
I_0         = 0.01  # начальная доля нифицированных особей
R_0         = 0     # начальная доля вылечившихся особей
#------------------

def SAIR_SDE(beg_conditions, times):
    a, i, r = beg_conditions
    return [
        growth/2 + sigma_RA*r + beta_SA*(1 - a - i - r) + sigma_AI*i - a*(sigma_SA + beta_AI + reduction),
        beta_AI*a + sigma_SI*(1 - a - i - r) - i*(sigma_SI + sigma_AI + sigma_IR + reduction),
        sigma_IR * (1 - r) - r * (sigma_RS + sigma_RA + reduction)
    ]

def solve_SDE(times, beg_conditions):
    return odeint(SAIR_SDE, beg_conditions, times) * pop_size

def get_susceptible_value(SDE_solution, index):
    return pop_size - SDE_solution[index, 0] - SDE_solution[index, 1] - SDE_solution[index, 2]

def get_susceptible_values(SDE_solution, solution_size):
    susceptible = []
    for i in range (0, solution_size):
        susceptible.append(get_susceptible_value(SDE_solution, i))
    return susceptible

def show_plot(times, susceptible, antidotal, infected, recovered):
    plt.plot(times, susceptible, color="orange")
    plt.plot(times, antidotal, color="blue")
    plt.plot(times, infected, color="red")
    plt.plot(times, recovered, color="green")
    plt.xlabel("Time")
    plt.ylabel("Population")
    plt.legend(["Susceptible", "Antidotal", "Infected", "Recovered"], loc=4)
    plt.grid(True)
    plt.show()

def main():
    times = np.linspace(0, TIME, TIME*100)
    beg_conditions = [A_0, I_0, R_0]
    SDE_solution = solve_SDE(times, beg_conditions)
    susceptible = get_susceptible_values(SDE_solution, times.size)
    antidotal = SDE_solution[:, 0]
    infected = SDE_solution[:, 1]
    recovered = SDE_solution[:, 2]
    show_plot(times, susceptible, antidotal, infected, recovered)

main()
