import math
import matplotlib.pyplot as plt

#beg_params
#------------------
NUM_OF_MEASURE = 9                                          # количество замеров
MEASURE_INTERVAL = 0.5                                      # интервал замеров
NUM_OF_ROWS = 3                                             # количество строк в таблице графиков
NUM_OF_COLUMNS = math.ceil(NUM_OF_MEASURE / NUM_OF_ROWS)    # количество столбцов в таблице графиков

X_STEP = 0.2                                                # шаг по координате X
T_STEP = 0.01                                               # шаг по координате T
BETA = 0.01                                               # коэффициент бета
U_0 = 2                                                     # начальное положение волны

L = 10.0                                                    # длина отрезка X
T = NUM_OF_MEASURE * MEASURE_INTERVAL                       # длина отрезка T

N = int(L/X_STEP)                                           # размерность X
M = int(T/T_STEP)                                           # размерность T
#------------------

def wave_func(x):
    delta = math.sqrt(12/U_0)
    # return math.pi - 4 * math.atan(math.e ** (-x))
    return U_0/(math.cosh((x - 3)/delta)**2)

def init_grid():
    grid = [[0] * N for i in range(M)]
    grid[0] = [wave_func(i * X_STEP) for i in range(N)]
    return grid

def fill_grid(grid):
    for i in range(1, M):
        eps, mu = (2, 2) if i == 1 else (1, 1)
        prev_layout = grid[i - 1] if i == 1 else grid[i - 2]
        for j in range(N): 
            grid[i][j] = prev_layout[j] - (T_STEP/X_STEP)/mu*grid[i - 1][j]*(grid[i - 1][(j + 1) % N] - grid[i - 1][(j - 1) % N])
            - BETA/eps*(T_STEP/(X_STEP**3))*(grid[i - 1][(j + 2) % N] - 2*grid[i - 1][(j + 1) % N] + 2*grid[i - 1][(j - 1) % N] - grid[i - 1][(j - 2) % N])

def find_layout_index(t):
    return int(t/T_STEP)

def get_wave_at_time(t, grid):
    layout = find_layout_index(t)
    return grid[layout][:]

def add_graph(i, cur_time, x_values, wave):
    plt.subplot(NUM_OF_ROWS, NUM_OF_COLUMNS, i + 1)
    plt.plot(x_values, wave, color="orange")
    plt.xlabel("x")
    plt.ylabel("u(x, t)")
    plt.title(round(cur_time, 2))
    plt.grid(True)

def draw_graphs():
    plt.show()

def main():
    grid = init_grid()
    fill_grid(grid)

    x_values = [i * X_STEP for i in range(N)]
    for i in range(NUM_OF_MEASURE):
        cur_time = i * MEASURE_INTERVAL
        wave = get_wave_at_time(cur_time, grid)
        add_graph(i, cur_time, x_values, wave)
    draw_graphs()

main()
